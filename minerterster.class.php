<?php
# ========================================================================#
#
#  Author:    Ruslan Mingaev
#  Version:   1.0
#  Date:      8-Oct-17
#  Purpose:   Query profitability of all algorithms on 
#             NiceHash and return index of most profitable algorithm.
#  
#
#  Usage Example:
#                     include("minerterster.class.php");
#                     $nicehash = new NiceHash();
#                     print_r($nicehash->getBest());
#
# ========================================================================#
class NiceHash {
	
	private $api_url = 'https://api.nicehash.com/api';
	private $cookie;
	private $user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:49.0) Gecko/20100101 Firefox/49.0';

	private $best_prof;
	private $current;
	private $info;

	public $Response;

	public function __construct() {
		$this->getCurrent();
		$this->simplemultialgo();
		$this->getParams();
	}

	public function getBest() {
		usort($this->best_prof, function($a, $b) {
			if ($a['prof'] == $b['prof']) return 0;
			return ($a['prof'] > $b['prof']) ? -1 : 1;
		});

		return $this->best_prof[0];
	}

	public function getParams() {
		$result = [];

		foreach ($this->current as $item) {
			$result[$item['algo']]['speed'] = $item['speed'];
		}

		foreach ($this->info as $item) {
			$result[$item['algo']]['port'] = $item['port'];
			$result[$item['algo']]['name'] = $item['name'];
			$result[$item['algo']]['algo'] = $item['algo'];
			$result[$item['algo']]['prof'] = $result[$item['algo']]['speed'] * $item['paying'];
		}

		$this->best_prof = $result;
	}

	public function getCurrent() {
		$this->makeRequest('stats.global.current',[], "GET");
		$this->current = $this->getJSON(true)['result']['stats'];
	}

	public function simplemultialgo() {
		$this->makeRequest('simplemultialgo.info',[], "GET");
		$this->info = $this->getJSON(true)['result']['simplemultialgo'];
	}

	public function getJSON($assoc = false) {
		return json_decode($this->Response, $assoc);
	}

	private function makeRequest($method, $data, $type = "POST", $timeout = 15) {
		if($type == "POST") {
			$request = curl_init("{$this->api_url}?method={$method}");
			curl_setopt($request, CURLOPT_POST, 1);
			curl_setopt($request, CURLOPT_POSTFIELDS, http_build_query($data));
		}elseif($type == "GET") {
			$request = curl_init("{$this->api_url}?method={$method}&".http_build_query($data));
			curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'GET');
		}

		curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($request, CURLOPT_COOKIESESSION, true);
		curl_setopt($request, CURLOPT_VERBOSE, 0);
		curl_setopt($request, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($request, CURLOPT_COOKIEFILE, $this->cookie);
		curl_setopt($request, CURLOPT_COOKIEJAR, $this->cookie);
		curl_setopt($request, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($request, CURLOPT_TIMEOUT, $timeout);
		$this->Response = curl_exec($request);
		curl_close($request);

		return true;
	}
}
?>